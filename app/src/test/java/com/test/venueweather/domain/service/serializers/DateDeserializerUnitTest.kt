package com.test.venueweather.domain.model

import com.google.gson.JsonElement
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.test.venueweather.domain.service.serializers.DateDeserializer
import org.junit.Test
import org.junit.Assert.*
import java.util.*

class DateDeserializerUnitTest {

    @Test
    fun deserialize_correctDateReturned() {
        val date = 1401666605L
        val dateSerializer = DateDeserializer()
        val jsonElement = mock<JsonElement> {
            on { asLong } doReturn date
        }
        assertEquals(Date(date * 1000), dateSerializer.deserialize(jsonElement, null, null))
    }

}