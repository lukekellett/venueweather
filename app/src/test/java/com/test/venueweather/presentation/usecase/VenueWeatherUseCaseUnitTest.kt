package com.test.venueweather.presentation.usecase

import org.junit.Test

class VenueWeatherUseCaseUnitTest {

    // In order to properly test use cases mocking of the service classes with a mock web server
    // would be required. But given the time frame available for this test that puts it beyond the
    // scope of the project. However in a large scale project this would be a requirement.
    @Test
    fun noTest() {
    }

}