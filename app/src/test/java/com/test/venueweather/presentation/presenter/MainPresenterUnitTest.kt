package com.test.venueweather.presentation.presenter

import com.nhaarman.mockito_kotlin.mock
import com.test.venueweather.domain.model.WeatherServiceModels
import com.test.venueweather.presentation.model.Venue
import com.test.venueweather.presentation.usecase.observer.VenueWeatherUseCaseObserver
import com.test.venueweather.presentation.usecase.VenueWeatherUseCase
import com.test.venueweather.presentation.view.MainView
import org.junit.Test
import org.junit.Before
import org.mockito.Mockito.verify


class MainPresenterUnitTest {

    lateinit var venueWeatherUseCase: VenueWeatherUseCase
    lateinit var venueWeatherUseCaseObserver: VenueWeatherUseCaseObserver
    lateinit var mainView: MainView
    lateinit var mainPresenter: MainPresenter

    @Before
    fun setup() {
        venueWeatherUseCase = mock<VenueWeatherUseCase> {}
        venueWeatherUseCaseObserver = mock<VenueWeatherUseCaseObserver> {}
        mainView = mock<MainView> {}
        mainPresenter = MainPresenter(venueWeatherUseCase, venueWeatherUseCaseObserver)
        mainPresenter.initialize(mainView)
    }

    @Test
    fun setVenues_dataSentToViewAndLoadingHidden() {

        // We don't really need/want a mutable list. Though all we are checking is that the object
        // is passed through correctly. So we use a convenience function, for well, convenience 😱.
        val venues = listOf<Venue>()
        mainPresenter.setVenues(venues)

        verify(mainView).hideLoading()
        verify(mainView).setData(venues)

    }

    @Test
    fun onResume_dataReloaded() {
        mainPresenter.onResume()
        verify(mainView).showLoading()
        verify(venueWeatherUseCase).execute(venueWeatherUseCaseObserver)
    }

    @Test
    fun refresh_dataReloaded() {
        mainPresenter.refresh()
        verify(mainView).showLoading()
        verify(venueWeatherUseCase).execute(venueWeatherUseCaseObserver)
    }

    @Test
    fun onPause_useCasesUnsubscribed() {
        mainPresenter.onPause()
        verify(venueWeatherUseCase).unsubscribe()
    }

    @Test
    fun onDestroy_useCasesDisposed() {
        mainPresenter.onDestroy()
        verify(venueWeatherUseCase).dispose()
    }

    @Test
    fun showError_loadingHiddenAndErrorDisplayed() {
        // In a large scale application the error from the usecase would likely be translated for
        // user comsumption in the UI layer. Here we'd check to make sure that translation occurred.
        // Which would likely occur at the base Presenter level and be routed through another UseCase
        // into the view.
        val throwable = Throwable()
        mainPresenter.showError(throwable)
        verify(mainView).hideLoading()
        verify(mainView).showError(throwable)
    }

}