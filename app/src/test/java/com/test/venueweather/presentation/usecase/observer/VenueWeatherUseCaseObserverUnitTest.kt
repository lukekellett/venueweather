package com.test.venueweather.presentation.usecase.observer

import android.content.Context
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.test.venueweather.domain.model.WeatherServiceModels
import com.test.venueweather.presentation.mapper.CountryDataMapper
import com.test.venueweather.presentation.mapper.VenueDataMapper
import com.test.venueweather.presentation.model.Venue
import com.test.venueweather.presentation.presenter.MainPresenter
import org.junit.Test
import org.junit.Before
import org.junit.Ignore
import java.util.*

class VenueWeatherUseCaseObserverUnitTest {

    lateinit var presenter: MainPresenter
    lateinit var venueWeatherUseCaseObserver: VenueWeatherUseCaseObserver
    lateinit var domainVenue1: WeatherServiceModels.Venue
    lateinit var domainVenue2: WeatherServiceModels.Venue
    lateinit var domainVenues: List<WeatherServiceModels.Venue>
    lateinit var presentationVenue1: Venue
    lateinit var presentationVenue2: Venue
    lateinit var presentationVenues: List<Venue>
    var presentationCountries = listOf<String>("countryName1", "countryName2")


    @Before
    fun setup() {
        presenter = mock<MainPresenter> {}
        venueWeatherUseCaseObserver = VenueWeatherUseCaseObserver(
                // While these are covered indirectly through the use case tests it would be more
                // ideal to have specific tests for each as well to cover things like specific errors.
                VenueDataMapper(),
                CountryDataMapper()
        )
        venueWeatherUseCaseObserver.presenter = presenter

        domainVenue1 = WeatherServiceModels.Venue("id", "name1", WeatherServiceModels.NamedElement("countryId", "countryName1"), "weatherConditions", "weatherConditionsIcon", "Wind: weatherWind", "Humidity: weatherHumidity", "42", "42", Date(1234567890), WeatherServiceModels.NamedElement("sportId", "sportName"))
        domainVenue2 = WeatherServiceModels.Venue("id", "name2", WeatherServiceModels.NamedElement("countryId", "countryName2"), "weatherConditions", "weatherConditionsIcon", "weatherWind", "weatherHumidity", "", "", Date(1234567890), WeatherServiceModels.NamedElement("sportId", "sportName"))
        domainVenues = listOf(domainVenue1, domainVenue2)
        presentationVenue1 = Venue("id", "name1", "countryId", "countryName1", "weatherConditions", "weatherConditionsIcon", "weatherWind", "weatherHumidity", "42°", 42, "42°", "Jan 15, 1970 04:56 PM", "sportId", "sportName", 1234567890)
        presentationVenue2 = Venue("id", "name2", "countryId", "countryName2", "weatherConditions", "weatherConditionsIcon", "weatherWind", "weatherHumidity", "-", 0,"-", "Jan 15, 1970 04:56 PM", "sportId", "sportName", 1234567890)
        presentationVenues = listOf(presentationVenue1, presentationVenue2)

    }

    @Test
    fun onError_showErrorCalledOnPresenter() {
        val throwable = Throwable()
        venueWeatherUseCaseObserver.onError(throwable)
        verify(presenter).showError(throwable)
    }

    @Test
    fun onNext_validData_setDataCalledOnPresenter() {
        // We don't really need/want a mutable list. Though all we are checking is that the object
        // is passed through correctly. So we use a convenience function, for well, convenience 😱.
        val response = WeatherServiceModels.VenueWeatherResponse(true, true, domainVenues)

        venueWeatherUseCaseObserver.onNext(response)
        verify(presenter).setVenues(presentationVenues)
        verify(presenter).setCountries(presentationCountries)

    }

    @Test
    fun onNext_invalidData_setDataCalledOnPresenter() {
        // We don't really need/want a mutable list. Though all we are checking is that the object
        // is passed through correctly. So we use a convenience function, for well, convenience 😱.
        val venues = listOf<WeatherServiceModels.Venue>()
        val response = WeatherServiceModels.VenueWeatherResponse(true, false, venues)

        venueWeatherUseCaseObserver.onNext(response)
        verify(presenter).showError(any())

    }

}