package com.test.venueweather.domain.repository

import com.test.venueweather.domain.model.WeatherServiceModels
import com.test.venueweather.domain.service.WeatherService
import io.reactivex.Observable


class WeatherRepository(val weatherService: WeatherService) {

    fun getVenueWeather(): Observable<WeatherServiceModels.VenueWeatherResponse> {
        return weatherService.getVenueWeather()
    }

}