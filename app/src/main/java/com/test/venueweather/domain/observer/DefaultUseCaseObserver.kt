package com.test.venueweather.domain.observer


// An observer that accepts all calls but does nothing with the data, by default.
open class DefaultUseCaseObserver<T>: UseCaseObserver<T>() {

    override fun onNext(result: T) {

    }

    override fun onError(error: Throwable) {

    }

    override fun onComplete() {

    }

}