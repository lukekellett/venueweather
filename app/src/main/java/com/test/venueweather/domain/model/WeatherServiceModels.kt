package com.test.venueweather.domain.model

import com.google.gson.annotations.SerializedName
import java.util.*


object WeatherServiceModels {

    data class VenueWeatherResponse(
            val ret: Boolean,
            val isOkay: Boolean,
            @SerializedName("data")
            val venues: List<Venue>
    )

    data class NamedElement(
            @SerializedName("_venueID", alternate =  arrayOf("_countryID", "_sportID"))
            val id: String,
            @SerializedName("_name", alternate =  arrayOf("_description"))
            val name: String
    )

    data class Venue (
            @SerializedName("_venueID")
            val id: String,
            @SerializedName("_name")
            val name: String,
            @SerializedName("_country")
            val country: NamedElement,
            @SerializedName("_weatherCondition")
            val weatherCondition: String,
            @SerializedName("_weatherConditionIcon")
            val weatherConditionIcon: String,
            @SerializedName("_weatherWind")
            val weatherWind: String,
            @SerializedName("_weatherHumidity")
            val weatherHumidity: String,
            @SerializedName("_weatherTemp")
            val weatherTemp: String,
            @SerializedName("_weatherFeelsLike")
            val weatherFeelsLike: String,
            @SerializedName("_weatherLastUpdated")
            val weatherLastUpdated: Date,
            @SerializedName("_sport")
            val sport: NamedElement
    )

}