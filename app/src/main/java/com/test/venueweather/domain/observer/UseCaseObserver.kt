package com.test.venueweather.domain.observer

abstract class UseCaseObserver<T> {

    abstract fun onNext(result: T)
    abstract fun onError(error: Throwable)
    abstract fun onComplete()

}