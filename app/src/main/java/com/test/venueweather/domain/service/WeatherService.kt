package com.test.venueweather.domain.service

import com.google.gson.GsonBuilder
import com.test.venueweather.domain.model.WeatherServiceModels
import com.test.venueweather.domain.service.serializers.DateDeserializer
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import java.util.*

interface WeatherService {

    @GET("/venues/weather.json")
    fun getVenueWeather(): Observable<WeatherServiceModels.VenueWeatherResponse>

    companion object {
        fun create(): WeatherService {

            val gsonBuilder = GsonBuilder()
            gsonBuilder.registerTypeAdapter(Date::class.java, DateDeserializer())

            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                    .baseUrl("http://dnu5embx6omws.cloudfront.net")
                    .build()

            return retrofit.create(WeatherService::class.java)

        }

    }

}