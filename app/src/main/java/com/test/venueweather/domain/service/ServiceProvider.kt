package com.test.venueweather.domain.service


object ServiceProvider {

    val provideWeatherService by lazy { WeatherService.create() }

}