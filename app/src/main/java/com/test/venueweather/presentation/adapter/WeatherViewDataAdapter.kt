package com.test.venueweather.presentation.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.test.venueweather.R
import com.test.venueweather.presentation.model.Venue
import kotlinx.android.synthetic.main.weather_view.*
import kotlinx.android.synthetic.main.weather_view.view.*
import android.support.annotation.IdRes


class WeatherViewDataAdapter(): RecyclerView.Adapter<WeatherViewDataAdapter.ViewHolder>() {

    var data: List<Venue> = listOf()
        set(value) {
            field = performSort(sortBy, value)
            notifyDataSetChanged()
        }
    @IdRes
    var sortBy: Int = R.id.SORT_AZ
        set(value) {
            if(lastSortBy == value) {
                invertSort != invertSort
            }
            field = value
            // Force re-sort
            data = data
        }
    private var lastSortBy: Int = 0
    private var invertSort = false

    var countryFilter: String? = null
    set(value) {
        field = value
        if(value != null) {
            data = performCountryFilter(value)
        }
    }

    class ViewHolder(var view: ViewGroup) : ExpandableViewHolder<Venue>(view) {
        override fun bind(venue: Venue) {
            super.bind(venue)
            view.temperature.text = venue.weatherTemp
            view.location.text = venue.name
            view.weatherCondition.text = venue.weatherCondition
            view.feelsLikeValue.text = venue.weatherFeelsLike
            view.windValue.text = venue.weatherWind
            view.humidityValue.text = venue.weatherHumidity
            view.timeStampValue.text = venue.weatherLastUpdated
            view.setOnClickListener(this)

        }

        override fun getExpandedHeight(): Int {
            return itemView.resources.getDimensionPixelSize(R.dimen.weather_view_height_expanded)
        }

        override fun getExpandedView(): View {
            return view.expandedView
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(parent!!.context).inflate(R.layout.weather_view, parent, false) as ViewGroup

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        var venue = data.get(position)
        holder?.bind(venue)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    // Should be run in a BG thread for performance
    // Also could be placed behind the use case with a filter parameter using a cache store
    private fun performCountryFilter(country: String): List<Venue> {
        return data.filter { it.countryName == country }
    }

    // Should be run in a BG thread for performance
    // Also could be placed behind the use case with a sort parameter using a cache store
    private fun performSort(@IdRes sortBy: Int, data: List<Venue>): List<Venue> {
        when(sortBy) {
            R.id.SORT_AZ -> {
                return if(invertSort) data.sortedByDescending { it.name } else data.sortedBy { it.name }
            }
            R.id.SORT_TEMP -> {
                return if(invertSort) data.sortedByDescending { it.weatherTempInt } else data.sortedBy { it.weatherTempInt }
            }
            R.id.SORT_DATE -> {
                return if(invertSort) data.sortedByDescending { it.weatherLastUpdatedTimeStamp } else data.sortedBy { it.weatherLastUpdatedTimeStamp }
            }
            else -> {
                return data
            }
        }

    }

}