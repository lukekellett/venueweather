package com.test.venueweather.presentation.mapper

import android.util.Log
import com.test.venueweather.domain.model.WeatherServiceModels
import javax.inject.Inject

class CountryDataMapper @Inject constructor(): DataMapper<WeatherServiceModels.Venue, String>() {

    private val TAG = CountryDataMapper::class.java.simpleName

    override fun uniqueOnly(): Boolean {
        return true
    }

    override fun transform(value: WeatherServiceModels.Venue): String? {
        try {
            return value.country.name
        } catch (exception: Exception) {
            Log.e(TAG, "Failed to map country from venue '${value}'")
            return null
        }
    }

}