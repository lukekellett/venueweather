package com.test.abcnews


interface View {
    fun showError(error: Throwable)
}