package com.test.venueweather.presentation.presenter

import com.test.abcnews.View


abstract class Presenter<T: View> {

    protected var view: T? = null
    private set

    open fun initialize(view: T) {
        this.view = view
    }

    open fun onResume() {

    }

    open fun onPause() {

    }

    open fun onDestroy() {

    }

    open fun showError(error: Throwable) {
        // Here we'd take non user friendly server errors and pass them through another use case
        // to get a user friendly message to show...
        view?.showError(error)
    }

}