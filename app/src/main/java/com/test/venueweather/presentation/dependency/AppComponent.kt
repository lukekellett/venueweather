package com.test.venueweather.presentation.dependency

import com.test.venueweather.presentation.MainActivity
import com.test.venueweather.presentation.usecase.observer.VenueWeatherUseCaseObserver
import com.test.venueweather.presentation.presenter.MainPresenter
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {
    fun inject(mainActivity: MainActivity)
    fun inject(mainPresenter: MainPresenter)
    fun inject(observer: VenueWeatherUseCaseObserver)
}