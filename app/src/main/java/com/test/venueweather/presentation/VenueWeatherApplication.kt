package com.test.venueweather.presentation

import android.app.Application
import com.test.venueweather.presentation.dependency.AppComponent
import com.test.venueweather.presentation.dependency.AppModule
import com.test.venueweather.presentation.dependency.DaggerAppComponent


class VenueWeatherApplication: Application() {

    private val appComponent: AppComponent = intializeDagger(this)

    override fun onCreate() {
        super.onCreate()
        _application = this
    }

    private fun intializeDagger(application: Application): AppComponent {
        return DaggerAppComponent.builder()
                .appModule(AppModule(application))
                .build()
    }

    fun getAppComponent(): AppComponent {
        return appComponent
    }

    companion object {
        protected var _application: VenueWeatherApplication? = null

        fun getApplication(): VenueWeatherApplication {
            return _application!!
        }

        fun getAppComponent(): AppComponent {
            return getApplication().getAppComponent()
        }

    }

}