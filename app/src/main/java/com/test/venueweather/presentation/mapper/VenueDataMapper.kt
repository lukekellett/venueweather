package com.test.venueweather.presentation.mapper

import android.content.Context
import android.util.Log
import com.test.venueweather.R
import com.test.venueweather.domain.model.WeatherServiceModels
import com.test.venueweather.presentation.model.Venue
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class VenueDataMapper @Inject constructor(): DataMapper<WeatherServiceModels.Venue, Venue>() {

    private val TAG = VenueDataMapper::class.java.simpleName

    override fun transform(value: WeatherServiceModels.Venue): Venue? {
        try {
            return Venue(
                    value.id,
                    value.name,
                    value.country.id,
                    value.country.name,
                    value.weatherCondition,
                    value.weatherConditionIcon,
                    stripStringPrefix(value.weatherWind),
                    stripStringPrefix(value.weatherHumidity),
                    transformTemperature(value.weatherTemp),
                    value.weatherTemp.toIntOrNull() ?: 0,
                    transformTemperature(value.weatherFeelsLike),
                    transformTimeStamp(value.weatherLastUpdated),
                    value.sport.id,
                    value.sport.name,
                    value.weatherLastUpdated.time
            )
        } catch (exception: Exception) {
            Log.e(TAG, "Failed to map venue '${value}'")
            return null
        }
    }

    private fun stripStringPrefix(value: String): String {
        return if(value.indexOf(':') > -1) value.substring(value.indexOf(':') + 1).trim() else value
    }

    private fun transformTimeStamp(date: Date): String {
        val sdf = SimpleDateFormat("MMM dd, yyyy hh:mm a") //Or whatever format fits best your needs.
        return sdf.format(date)
    }

    private fun transformTemperature(value: String): String {
        // For the sake simplicity and speed in setting up tests these have been hard coded
        // (i.e. if using strings.xml a context would be needed and that would mean instrumented
        // tests to be set up also.)
        if(value.isNullOrBlank()) {
            return "-"//context.getString(R.string.temperature_blank)
        } else {
            return "$value°"//context.getString(R.string.temperature_format, value)
        }
    }

}