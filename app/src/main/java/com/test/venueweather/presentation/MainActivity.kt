package com.test.venueweather.presentation

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.test.venueweather.R
import com.test.venueweather.R.id.filter_menu_clear_id
import com.test.venueweather.R.id.filter_menu_group_id
import com.test.venueweather.presentation.adapter.WeatherViewDataAdapter
import com.test.venueweather.presentation.adapter.decorator.EqualSpacingItemDecoration
import com.test.venueweather.presentation.model.Venue
import com.test.venueweather.presentation.presenter.MainPresenter
import com.test.venueweather.presentation.view.MainView
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(), MainView {

    @Inject
    lateinit var presenter: MainPresenter
    private var adapter = WeatherViewDataAdapter()
    private var countriesInternal = listOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        initialize()
    }

    private fun initialize() {
        VenueWeatherApplication.getAppComponent().inject(this)
        swipeRefreshLayout?.setOnRefreshListener {
            clearFilter(false)
            presenter.refresh()
        }

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
        recyclerView.addItemDecoration(EqualSpacingItemDecoration(resources.getDimensionPixelSize(R.dimen.spacing_large)))

        presenter.initialize(this)

    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        var filterMenu = menu!!.findItem(R.id.menu_filter)

        // Create an options menu for each unique country we've found
        if(filterMenu.hasSubMenu()) {
            var filterMenuSubMenu = filterMenu.subMenu
            filterMenuSubMenu.clear()
            countriesInternal.forEach {
                var menuItem = filterMenuSubMenu.add(R.id.filter_menu_group_id, Menu.NONE, Menu.NONE, it)
                menuItem.isCheckable = true
                menuItem.isChecked = adapter.countryFilter == it
            }
            // Plus one to specifically clear the filter and reload the data but only when a filter
            // isn't already applied
            if(adapter.countryFilter != null) {
                filterMenuSubMenu.add(Menu.NONE, R.id.filter_menu_clear_id, Menu.NONE, getString(R.string.menu_filter_clear))
            }

        }

        return super.onPrepareOptionsMenu(menu)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        toggleSortMenuChecks(adapter.sortBy)
        return true
    }

    private fun toggleSortMenuChecks(sortBy: Int) {
        toolbar.menu.findItem(R.id.SORT_AZ)?.isChecked = false
        toolbar.menu.findItem(R.id.SORT_TEMP)?.isChecked = false
        toolbar.menu.findItem(R.id.SORT_DATE)?.isChecked = false
        // Check the last one selected
        toolbar.menu.findItem(sortBy)?.isChecked = true

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if(item!!.groupId == filter_menu_group_id) {
            item.isChecked = true
            adapter.countryFilter = item.title.toString()
            setCountries(listOf(item.title.toString()))
            return true
        }

        when(item.itemId) {
            R.id.SORT_AZ, R.id.SORT_TEMP, R.id.SORT_DATE -> {
                toggleSortMenuChecks(item.itemId)
                // Toggle all checkable menu items as unchecked
                adapter.sortBy = item.itemId
                return true
            }
            filter_menu_clear_id -> {
                clearFilter()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun clearFilter(forceRefresh: Boolean = true) {
        adapter.countryFilter = null
        if(forceRefresh) {
            presenter.refresh()
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    override fun onPause() {
        super.onPause()
        presenter.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun setData(data: List<Venue>) {
        adapter.data = data
    }

    override fun setCountries(countries: List<String>) {
        countriesInternal = countries
        invalidateOptionsMenu()
    }

    override fun showError(error: Throwable) {
        var builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.error_title_request)
        builder.setMessage(error.message)
        builder.setPositiveButton(R.string.button_ok, null)
        builder.setCancelable(true)
        builder.create().show()
    }

    override fun showLoading() {
        clearFilter(false)
        swipeRefreshLayout?.isRefreshing = true
    }

    override fun hideLoading() {
        swipeRefreshLayout?.isRefreshing = false
    }

}
