package com.test.venueweather.presentation.usecase

import com.test.venueweather.domain.observer.UseCaseObserver
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

abstract class UseCase<T> {

    private var disposable: Disposable? = null
    private var useCaseObservable: Observable<T>? = null

    abstract fun getUseCaseObservable(): Observable<T>

    fun execute(observer: UseCaseObserver<T>) {
        unsubscribe()
        useCaseObservable = getUseCaseObservable()
        disposable = useCaseObservable!!
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    observer.onNext(result)
                }, { error ->
                    observer.onError(error)
                }, {
                    observer.onComplete()
                })

    }

    fun unsubscribe() {
        useCaseObservable?.unsubscribeOn(Schedulers.io())
        useCaseObservable = null

    }

    fun dispose() {
        unsubscribe()
        disposable?.dispose()
        disposable = null
    }

}