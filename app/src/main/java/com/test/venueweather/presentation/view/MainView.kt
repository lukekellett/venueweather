package com.test.venueweather.presentation.view

import com.test.abcnews.DataView
import com.test.venueweather.presentation.model.Venue

interface MainView: DataView<List<Venue>> {
    fun setCountries(countries: List<String>)
}