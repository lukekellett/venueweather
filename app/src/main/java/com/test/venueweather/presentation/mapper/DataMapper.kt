package com.test.venueweather.presentation.mapper


abstract class DataMapper<I,O> {

    abstract fun transform(value: I): O?

    open protected fun uniqueOnly(): Boolean {
        return false
    }

    fun transform(values: List<I>): List<O> {
        var results = mutableListOf<O>()

        // If we knew this was a one to one conversion we could just use .map. But we don't so we can't.
        values.forEach {
            var result = transform(it)

            if(result != null && (!uniqueOnly() || !results.contains(result))) {
                results.add(result)

            }

        }

        return results

    }

}