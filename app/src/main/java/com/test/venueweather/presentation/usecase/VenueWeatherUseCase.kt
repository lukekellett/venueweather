package com.test.venueweather.presentation.usecase

import com.test.venueweather.domain.model.WeatherServiceModels
import com.test.venueweather.domain.service.ServiceProvider
import io.reactivex.Observable
import javax.inject.Inject

class VenueWeatherUseCase @Inject constructor(): UseCase<WeatherServiceModels.VenueWeatherResponse>() {

    override fun getUseCaseObservable(): Observable<WeatherServiceModels.VenueWeatherResponse> {
        return ServiceProvider.provideWeatherService.getVenueWeather()
    }

}