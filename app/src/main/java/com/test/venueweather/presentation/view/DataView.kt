package com.test.abcnews


interface DataView<T>: View {

    fun showLoading()
    fun hideLoading()
    fun setData(data: T)

}