package com.test.venueweather.presentation.model

import java.util.Date

data class Venue(
        val id: String,
        val name: String,
        val countryId: String,
        val countryName: String,
        val weatherCondition: String,
        val weatherConditionIcon: String,
        val weatherWind: String,
        val weatherHumidity: String,
        val weatherTemp: String,
        val weatherTempInt: Int,
        val weatherFeelsLike: String,
        val weatherLastUpdated: String,
        val sportId: String,
        val sportName: String,
        val weatherLastUpdatedTimeStamp: Long
)