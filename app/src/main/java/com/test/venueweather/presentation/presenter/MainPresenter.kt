package com.test.venueweather.presentation.presenter

import com.test.venueweather.presentation.model.Venue
import com.test.venueweather.presentation.usecase.VenueWeatherUseCase
import com.test.venueweather.presentation.usecase.observer.VenueWeatherUseCaseObserver
import com.test.venueweather.presentation.view.MainView
import javax.inject.Inject


class MainPresenter @Inject constructor(
        private val venueWeatherUseCase: VenueWeatherUseCase,
        private val venueWeatherUseCaseObserver: VenueWeatherUseCaseObserver
): Presenter<MainView>() {


    override fun initialize(view: MainView) {
        super.initialize(view)
        venueWeatherUseCaseObserver.presenter = this
    }

    fun setVenues(venus: List<Venue>) {
        view?.setData(venus)
        view?.hideLoading()
    }

    fun setCountries(countries: List<String>) {
        view?.setCountries(countries)
    }

    override fun onResume() {
        super.onResume()
        refresh()
    }

    fun refresh() {
        loadData()
    }

    private fun loadData() {
        view?.showLoading()
        venueWeatherUseCase.execute(venueWeatherUseCaseObserver)
    }

    override fun onPause() {
        super.onPause()
        view?.hideLoading()
        venueWeatherUseCase.unsubscribe()
    }

    override fun showError(error: Throwable) {
        super.showError(error)
        view?.hideLoading()
    }

    override fun onDestroy() {
        super.onDestroy()
        venueWeatherUseCase.dispose()
    }

}