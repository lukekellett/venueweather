package com.test.venueweather.presentation.usecase.observer

import com.test.venueweather.domain.model.WeatherServiceModels
import com.test.venueweather.domain.observer.DefaultUseCaseObserver
import com.test.venueweather.presentation.mapper.CountryDataMapper
import com.test.venueweather.presentation.mapper.VenueDataMapper
import com.test.venueweather.presentation.presenter.MainPresenter
import javax.inject.Inject

class VenueWeatherUseCaseObserver
    @Inject
    constructor(
            private val venueDataMapper: VenueDataMapper,
            private val countryDataMapper: CountryDataMapper
    ) : DefaultUseCaseObserver<WeatherServiceModels.VenueWeatherResponse>() {

    var presenter: MainPresenter? = null

    override fun onError(error: Throwable) {
        presenter?.showError(error)
    }

    override fun onNext(result: WeatherServiceModels.VenueWeatherResponse) {
        super.onNext(result)

        if(result.isOkay) {
            // Here we'd do any extra data processing needed for the UI, through a mapper dependency. At
            // this point we'd also use a UI model instead of a domain model.
            presenter?.setCountries(countryDataMapper.transform(result.venues))
            presenter?.setVenues(venueDataMapper.transform(result.venues))
        } else {
            presenter?.showError(Throwable("Invalid data"))
        }

    }


}