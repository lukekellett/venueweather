package com.test.venueweather.presentation.adapter

import android.animation.ValueAnimator
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.AlphaAnimation
import android.view.animation.Animation


abstract class ExpandableViewHolder<T>(itemView: View): RecyclerView.ViewHolder(itemView), View.OnClickListener {

    private val ANIMATION_DURATION = 200L

    private var isExpanded = false
    private var originalHeight: Int? = null

    abstract fun getExpandedHeight(): Int
    abstract fun getExpandedView(): View

    open fun bind(data: T) {
        isExpanded = false
        val expandedView = getExpandedView()
        expandedView.setVisibility(View.GONE)
        expandedView.setEnabled(false)

        // Ensure that we don't show as expanded if we haven't been expanded
        if(originalHeight != null) {
            itemView.layoutParams.height = originalHeight!!
            itemView.requestLayout()

        }

    }

    override fun onClick(view: View?) {
        // If the originalHeight is 0 then find the height of the View being used
        // This would be the height of the cardview
        if(originalHeight == null) {
            originalHeight = view?.getHeight()
        }

        if(originalHeight == null) {
            return
        }

        // Declare a ValueAnimator object
        val valueAnimator: ValueAnimator
        var expandedView = getExpandedView()

        if(!isExpanded) {
            expandedView.setVisibility(View.VISIBLE)
            expandedView.setEnabled(true)
            expandedView.clearAnimation()

            val alphaAnimation = AlphaAnimation(0.00f, 1.00f) // Fade in
            alphaAnimation.duration = ANIMATION_DURATION
            expandedView.startAnimation(alphaAnimation)

            isExpanded = true
            valueAnimator = ValueAnimator.ofInt(originalHeight!!, getExpandedHeight()) // These values in this method can be changed to expand however much you like

        } else {
            isExpanded = false
            valueAnimator = ValueAnimator.ofInt(getExpandedHeight(), originalHeight!!)

            val alphaAnimation = AlphaAnimation(1.00f, 0.00f) // Fade out
            alphaAnimation.duration = ANIMATION_DURATION

            // Set a listener to the animation and configure onAnimationEnd
            alphaAnimation.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation) {

                }

                override fun onAnimationEnd(animation: Animation) {
                    expandedView.setVisibility(View.GONE)
                    expandedView.setEnabled(false)
                }

                override fun onAnimationRepeat(animation: Animation) {

                }

            })

            // Set the animation on the custom view
            expandedView.startAnimation(alphaAnimation)

        }

        valueAnimator.duration = ANIMATION_DURATION
        valueAnimator.interpolator = AccelerateDecelerateInterpolator()
        valueAnimator.addUpdateListener { animation ->
            val value = animation.animatedValue as Int
            view!!.layoutParams.height = value
            view!!.requestLayout()
        }
        valueAnimator.start()
    }


}